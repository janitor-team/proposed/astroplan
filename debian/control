Source: astroplan
Section: python
Priority: optional
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Vincent Prat <vivi@debian.org>
Build-Depends: debhelper (>= 13),
               debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-astropy,
               python3-astropy-helpers,
               python3-matplotlib,
               python3-pytest,
               python3-setuptools,
               python3-sphinx,
               python3-tk,
               xauth,
               xvfb
Standards-Version: 4.5.0
Homepage: https://pypi.python.org/pypi/astroplan/
Vcs-Browser: https://salsa.debian.org/debian-astro-team/astroplan
Vcs-Git: https://salsa.debian.org/debian-astro-team/astroplan.git
Rules-Requires-Root: no

Package: python3-astroplan
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Suggests: python-astroplan-doc
Description: Observation planning package for astronomers (Python 3)
 Astroplan is an observation planning package for astronomers that can help
 you plan for everything but the clouds.
 .
 It is an Astropy affiliated package that seeks to make your life as an
 observational astronomer a little less infuriating.
 .
 This is the Python 3 version of the package.

Package: python-astroplan-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: Observation planning package for astronomers (documentation)
 Astroplan is an observation planning package for astronomers that can help
 you plan for everything but the clouds.
 .
 It is an Astropy affiliated package that seeks to make your life as an
 observational astronomer a little less infuriating.
 .
 This is the common documentation package.
