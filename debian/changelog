astroplan (0.7-1) unstable; urgency=medium

  * New upstream release (Closes: #973348)

 -- Vincent Prat <vivi@debian.org>  Thu, 29 Oct 2020 17:16:37 +0100

astroplan (0.6-3) unstable; urgency=medium

  * Use Debian email address as uploader (d/control)
  * New patch to fix failing test (Closes: #971120)
  * Bump debhelper compatibility version to 13

 -- Vincent Prat <vivi@debian.org>  Sat, 03 Oct 2020 16:43:24 +0200

astroplan (0.6-2) unstable; urgency=medium

  * Fix intersphinx KeyError
  * Update Standards-Version to 4.5.0

 -- Vincent Prat <vinceprat@free.fr>  Mon, 30 Mar 2020 11:59:40 +0200

astroplan (0.6-1) unstable; urgency=medium

  * New upstream release
  * Update Standards-Version to 4.4.1
  * Add Rules-Requires-Root statement
  * Remove debian/compat file and add dependency to debhelper-compat
  * Re-enable tests at build time

 -- Vincent Prat <vinceprat@free.fr>  Fri, 20 Dec 2019 19:39:42 +0100

astroplan (0.5-2) unstable; urgency=medium

  * Allow warnings in CI tests

 -- Vincent Prat <vinceprat@free.fr>  Thu, 19 Sep 2019 09:44:13 +0200

astroplan (0.5-1) unstable; urgency=medium

  * New upstream version (Closes: #932688)
  * Drop outdated patches
  * Temporarily disable tests at build time
  * Update Standards-Version to 4.4.0
  * Update debhelper compatibility version to 12

 -- Vincent Prat <vinceprat@free.fr>  Wed, 18 Sep 2019 16:24:54 +0200

astroplan (0.4-4) unstable; urgency=medium

  * Drop Python 2 support (Closes: #916526)
  * Update Standards-Version to 4.2.1

 -- Vincent Prat <vinceprat@free.fr>  Sat, 15 Dec 2018 17:43:05 +0100

astroplan (0.4-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4
  * Update VCS links to Salsa
  * Remove python-version fields
  * New patch to fix doctest failures
  * New d/upstream/metadata file

 -- Vincent Prat <vinceprat@free.fr>  Sat, 26 May 2018 15:34:41 +0200

astroplan (0.4-2) unstable; urgency=medium

  * Disable assertion failing without any reason (Closes: #887446)
  * Update debhelper compatibility version to 11
  * Update Standard-Version to 4.1.3
  * Update copyright file and use secure URI

 -- Vincent Prat <vinceprat@free.fr>  Wed, 31 Jan 2018 11:32:53 +0100

astroplan (0.4-1) unstable; urgency=medium

  * New upstream release
  * Update Standards-Version to 4.1.1
  * Use secure URI in watch file

 -- Vincent Prat <vinceprat@free.fr>  Mon, 30 Oct 2017 13:04:01 +0100

astroplan (0.3-2) unstable; urgency=medium

  * Replace build dependency on python-sphinx with python3-sphinx
  * Update Standards-Version to 4.1.0
  * Remove Testsuite field from control file

 -- Vincent Prat <vinceprat@free.fr>  Tue, 26 Sep 2017 09:45:50 +0200

astroplan (0.3-1) unstable; urgency=medium

  * New upstream release
  * Drop patch fix_astropy2.0, fixed upstream
  * Add pgp signature verification

 -- Vincent Prat <vinceprat@free.fr>  Tue, 05 Sep 2017 16:19:10 +0100

astroplan (0.2.1-2) unstable; urgency=medium

  * Patch to fix test failure (Closes: #868116)
  * Tests now run for all supported versions of Python

 -- Vincent Prat <vinceprat@free.fr>  Wed, 12 Jul 2017 14:02:55 +0200

astroplan (0.2.1-1) unstable; urgency=medium

  * New upstream version
  * Drop patches for bugs that are fixed upstream
  * Update Standards-Version to 4.0.0
  * Update debhelper compatibility version to 10
  * Update watch file version to 4
  * Use SOURCE_DATE_EPOCH instead of dpkg-parsechangelog

 -- Vincent Prat <vinceprat@free.fr>  Sun, 25 Jun 2017 17:26:54 +0200

astroplan (0.2-5) unstable; urgency=medium

  * Fix broadcasts in schedulers (Closes: #855477)

 -- Vincent Prat <vinceprat@free.fr>  Sat, 18 Feb 2017 16:37:34 +0100

astroplan (0.2-4) unstable; urgency=medium

  * Github patches + failures marked as known (Closes: #851437)

 -- Vincent Prat <vinceprat@free.fr>  Fri, 27 Jan 2017 20:57:06 +0100

astroplan (0.2-3) unstable; urgency=medium

  * Fix test failures. (Closes: #848750)

 -- Vincent Prat <vinceprat@free.fr>  Wed, 21 Dec 2016 13:25:25 +0100

astroplan (0.2-2) unstable; urgency=medium

  * Fix CI tests
  * python-astroplan-doc marked as Multi-Arch: foreign

 -- Vincent Prat <vinceprat@free.fr>  Fri, 11 Nov 2016 18:20:46 +0100

astroplan (0.2-1) unstable; urgency=medium

  * Initial release. (Closes: #838842)

 -- Vincent Prat <vinceprat@free.fr>  Mon, 26 Sep 2016 23:03:21 +0200
